//! Types used by all stages of the compiler

use codespan::ByteIndex;
pub use codespan::Span;

pub mod error;
pub use error::Error;
pub use error::FileId;

pub type InternerID = lasso::MiniSpur;
pub type Interner = lasso::Rodeo<InternerID>;

/// A span of text in a specific file
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Location(Span);

impl Location {
    /// Create a new location from the specified span
    pub fn new(span: Span) -> Location {
        Location(span)
    }

    /// Return the inner span
    pub fn span(&self) -> Span {
        self.0
    }

    /// Return the inner file id
    pub fn file_id(&self) -> error::FileId {
        ()
    }

    /// Construct a location from an index in the file
    pub fn from_position(_file: FileId, position: ByteIndex) -> Location {
        Location::new(Span::new(position, position))
    }

    // fn range(&self) -> Range<usize> {

    // }

    /// Attach data to this location and return a [`Locatable`]
    pub fn with<T>(&self, data: T) -> Locatable<T> {
        Locatable::new(data, *self)
    }
}

pub trait IntoLocation {
    fn into_location(self) -> Location;
}

impl<T: Into<Location>> IntoLocation for T {
    fn into_location(self) -> Location {
        self.into()
    }
}

// TODO: When changing the File ID, change this too.
impl From<(FileId, ByteIndex, ByteIndex)> for Location {
    fn from(indecies: (FileId, ByteIndex, ByteIndex)) -> Location {
        Location::new(Span::new(indecies.1, indecies.2))
    }
}

/// Any data associated with a location
///
/// The primary way to construct a `Locatable` is with [`Location::with`]
#[derive(Debug, Clone, Copy)]
pub struct Locatable<T> {
    pub data: T,
    pub location: Location,
}

impl<T> Locatable<T> {
    /// Construct a Locatable form the location and contained data
    fn new(data: T, location: Location) -> Locatable<T> {
        Locatable { data, location }
    }

    /// Turn the Locatable into the structure needed by `lalrpop`
    pub fn to_spanned(self) -> (ByteIndex, T, ByteIndex) {
        (
            self.location.span().start(),
            self.data,
            self.location.span().end(),
        )
    }

    // pub fn map<U>(self, f: impl FnOnce(T) -> U) -> Locatable<U> {
    //     self.location.with(f(self.data))   
    // }
}

impl<T> From<(ByteIndex, T, ByteIndex)> for Locatable<T> {
    fn from(spanned: (ByteIndex, T, ByteIndex)) -> Locatable<T> {
        let (start, data, end) = spanned;
        Location::new(Span::new(start, end)).with(data)
    }
}