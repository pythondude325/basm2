//! Error types for code generation

use crate::common::{error::CompilerError, error::Diagnostic, Interner, InternerID, Location};

/// Errors produced during code generation
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SemanticError {
    /// A variable was defined twice
    DoubleDelcaration {
        loc1: Location,
        loc2: Location,
        name: InternerID,
    },
    /// A variable was used but not defined
    UndefinedVariable {
        loc: Location,
        name: InternerID,
    },
}

/// Result type used by code generation
pub type Result<T> = core::result::Result<T, SemanticError>;

impl CompilerError for SemanticError {
    fn into_diagnostic(&self, interner: &Interner) -> Diagnostic {
        use crate::common::error::diagnostic_utils::*;

        match self {
            SemanticError::DoubleDelcaration { loc1, loc2, name } => Diagnostic::error()
                .with_message(format!("`{}` was redefined", interner.resolve(name)))
                .with_labels(vec![
                    make_secondary(*loc1).with_message("previous definition here"),
                    make_primary(*loc2).with_message("redefined here"),
                ]),
            SemanticError::UndefinedVariable { loc, name } => Diagnostic::error()
                .with_message(format!(
                    "`{}` was used but never defined",
                    interner.resolve(name)
                ))
                .with_labels(vec![make_primary(*loc)]),
        }
    }
}
