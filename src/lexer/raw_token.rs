//! Raw tokens

use logos::Logos;

/// Raw tokens produced directly by the [`logos`] lexer
#[derive(Logos, Debug, PartialEq, Clone, Copy)]
pub enum RawToken {
    #[error]
    #[regex(r";.*", logos::skip)]
    #[regex(r"[ \t\f]+", logos::skip)]
    Error,

    #[regex(r"\n")]
    Newline,

    // #[regex(r"\p{XID_Start}\p{XID_Continue}*")] // If you don't like emojis
    #[regex(r"[\p{XID_Start}\p{Emoji_Presentation}][\p{XID_Continue}\p{Emoji_Presentation}]*")]
    Ident,

    #[regex("(?i)const")]
    Const,

    #[regex("(?i)db")]
    Db,

    #[token(":")]
    Colon,
    #[token(",")]
    Comma,
    #[token("+")]
    Plus,
    #[token("=")]
    Equal,
    #[token("(")]
    OpenParen,
    #[token(")")]
    CloseParen,
    #[token("[")]
    OpenBracket,
    #[token("]")]
    CloseBracket,

    #[regex("(?i)0b[01_]+")]
    BinaryLiteral,
    #[regex("(?i)0o[0-7_]+")]
    OctalLiteral,
    #[regex("[0-9_]+")]
    DecimalLiteral,
    #[regex("(?i)0x[0-9a-f_]+")]
    HexLiteral,

    #[regex(
        r#"(?x)
        "(
            (\\.)
            |([^\\"\n])
        )*"
        "#
    )]
    StringLiteral,

    #[regex("(?i)-n?c?z?")]
    ConditionFlag,

    // Register keywords
    #[regex("(?i)stack")]
    StackRegister,
    #[regex("(?i)ac")]
    ARegister,
    #[regex("(?i)br")]
    BRegister,
    #[regex("(?i)ix")]
    IndexRegister,
    #[regex("(?i)sp")]
    StackPointerRegister,

    // Mnemonics
    #[regex("(?i)mov")]
    Mov,
    #[regex("(?i)sub")]
    Sub,
    #[regex("(?i)sbb")]
    Sbb,
    #[regex("(?i)cmp")]
    Cmp,
    #[regex("(?i)add")]
    Add,
    #[regex("(?i)adc")]
    Adc,
    #[regex("(?i)sbw")]
    Sbw,
    #[regex("(?i)swb")]
    Swb,
    #[regex("(?i)nnd")]
    Nnd,
    #[regex("(?i)and")]
    And,
    #[regex("(?i)aib")]
    Aib,
    #[regex("(?i)anb")]
    Anb,
    #[regex("(?i)bia")]
    Bia,
    #[regex("(?i)bna")]
    Bna,
    #[regex("(?i)ora")]
    Ora,
    #[regex("(?i)nor")]
    Nor,
    #[regex("(?i)jmp")]
    Jmp,
    #[regex("(?i)jsr")]
    Jsr,
    #[regex("(?i)dec")]
    Dec,
    #[regex("(?i)inc")]
    Inc,
    #[regex("(?i)xor")]
    Xor,
    #[regex("(?i)xnr")]
    Xnr,
    #[regex("(?i)hlt")]
    Hlt,
    #[regex("(?i)ret")]
    Ret,
    #[regex("(?i)clc")]
    Clc,
    #[regex("(?i)clz")]
    Clz,
    #[regex("(?i)sec")]
    Sec,
    #[regex("(?i)sez")]
    Sez,
}
