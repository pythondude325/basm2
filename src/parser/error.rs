//! Error types for the Parser

use indoc::indoc;
use crate::common::{error::Diagnostic, Location, Interner, error::CompilerError};

/// Errors produced during parsing
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SyntaxError {
    /// A binary operation was not in a valid form.
    ///
    /// For binary operations to be correct, they must have at least one
    /// operand be `Ac`.
    /// 
    /// Examples:
    /// - `sub ac, 5`
    /// - `sub br, ac`
    /// - `cmp ac, ac`
    BinaryInstructionInvalidOperand(Location),
    /// A `mov` operation was not in valid form.
    ///
    /// `mov` operations must have one of two forms:
    /// - Normal `mov` operations, which have two normal operands.
    /// - Reference `mov` operations, which have *one* argument in square
    ///   brakets to indicate a reference. These operations may also be indexed
    ///   references.
    /// 
    /// Examples:
    /// - `mov ac, 5`
    /// - `mov ac, [5]`
    /// - `mov [3], (br)`
    /// - `mov [0xF0 + ix], stack`
    ///
    /// Incorrect form:
    /// - `mov [Ac], [Br]`
    InvalidMovForm(Location),
}

impl CompilerError for SyntaxError {
    fn into_diagnostic(&self, _interner: &Interner) -> Diagnostic {
        use crate::common::error::diagnostic_utils::*;

        match self {
            SyntaxError::InvalidMovForm(loc) => Diagnostic::error()
                .with_message("Invalid `mov` form")
                .with_labels(vec![make_primary(*loc)])
                .with_notes(vec![indoc!{r#"
                    `mov` must have one of two forms:
                    - No square brackets (`mov (0), (1)`)
                    - Square brakcet reference on one operand and a register for the other (`mov ac, [(0)]`)
                "#}.to_owned()]),

            SyntaxError::BinaryInstructionInvalidOperand(loc) => Diagnostic::error()
                .with_message("Invalid binary operation")
                .with_labels(vec![make_primary(*loc)])
                .with_notes(vec![indoc!{r#"
                    Binary operations require one operand to be the A register.
                "#}.to_owned()]),
        }
    }
}
