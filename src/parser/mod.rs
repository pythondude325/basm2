//! Parser and related types

mod error;
pub use error::SyntaxError;

pub mod ast;

lalrpop_mod!(pub basm, "/parser/basm.rs");

/// The error type produced by the `lalrpop` parser
pub type ParseError =
    lalrpop_util::ParseError<codespan::ByteIndex, crate::lexer::Token, crate::common::Error>;
