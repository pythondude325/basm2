#![warn(missing_docs)]
#![warn(clippy::missing_docs_in_private_items)]

//! # BASM
//!
//! Basm is the assember for the W-4096 12-bit computer designed by
//! [Buzz](https://github.com/Runnerguy1234).
//!
//! The assembly language syntax was co-designed with Buzz, but the instruction
//! set is completely his design.

use std::{
    fs,
    io::{self, Read},
    path::PathBuf,
};

use anyhow::{anyhow, Context};
use codespan_reporting::files::SimpleFile;
use structopt::StructOpt;

#[macro_use]
extern crate lalrpop_util;

pub(crate) mod codegen;
pub(crate) mod common;
pub(crate) mod lexer;
pub(crate) mod parser;

#[derive(Debug, StructOpt)]
#[structopt(name = "basm")]
struct Opt {
    /// Output file, or stdout if not present
    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,

    /// Don't pad the binary to 4096 words
    #[structopt(long = "no-padding")]
    no_padding: bool,

    /// Output code in binary format (each word as a little-endian 16-bit integer)
    /// instead of text format (one word per line in octal)
    #[structopt(short, long = "binary")]
    binary_mode: bool,

    /// Input file, or stdin if not present
    input: Option<PathBuf>,
}
fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    let input_file = if let Some(input_path) = &opt.input {
        fs::read_to_string(input_path).context("Unable to read input file")?
    } else {
        let mut buf = String::new();
        io::stdin()
            .read_to_string(&mut buf)
            .context("Unable to read stdin")?;
        buf
    };

    let input_file_name = if let Some(input_path) = &opt.input {
        input_path.to_string_lossy()
    } else {
        std::borrow::Cow::from("<stdin>")
    };

    let mut bitcode = compile(&input_file, &input_file_name)?;

    let mut output: Box<dyn io::Write> = if let Some(output_path) = &opt.output {
        Box::new(fs::File::create(output_path)?)
    } else {
        Box::new(io::stdout())
    };

    if !opt.no_padding {
        bitcode.resize(4096, 0);
    }

    if opt.binary_mode {
        for word in &bitcode {
            output
                .write_all(&word.to_le_bytes())
                .context("failed to write output")?;
        }
    } else {
        for word in &bitcode {
            write!(output, "{:04o}\n", word).context("failed to write output")?;
        }
    }

    output.flush().context("unable to flush output")?;

    return Ok(());
}

fn compile(input_file: &str, input_file_name: &str) -> anyhow::Result<Vec<u16>> {
    let file = SimpleFile::new(input_file_name, input_file);
    let file_id = ();
    let mut interner = common::Interner::new();

    let lexer = lexer::Lexer::new(file.source(), &mut interner);

    let mut parse_errors = Vec::new();

    let parse_result = parser::basm::ProgramParser::new().parse(
        file_id,
        &mut parse_errors,
        lexer.map(|r| r.map_err(common::Error::from)),
    );

    if !parse_errors.is_empty() {
        display_errors(
            parse_errors
                .into_iter()
                .map(|r| common::Error::from((file_id, r.error))),
            &file,
            &interner,
        );
        return Err(anyhow!("Parse errors"));
    }

    let ast = match parse_result {
        Ok(ast) => ast,
        Err(err) => {
            display_errors(std::iter::once((file_id, err).into()), &file, &interner);
            return Err(anyhow!("Parse errors"));
        }
    };

    let codegen_result = codegen::Generator::generate(ast, &interner);

    let bitcode = match codegen_result {
        Ok(bitcode) => bitcode,
        Err(err) => {
            display_errors(std::iter::once(err.into()), &file, &interner);
            return Err(anyhow!("Codegen errors"));
        }
    };

    return Ok(bitcode);
}

fn display_errors<'f, F>(
    errors: impl IntoIterator<Item = common::Error>,
    files: &'f F,
    interner: &common::Interner,
) where
    F: codespan_reporting::files::Files<'f, FileId = common::error::FileId>,
{
    use codespan_reporting::term::termcolor;
    use common::error::CompilerError;

    let writer = termcolor::StandardStream::stderr(termcolor::ColorChoice::Always);
    let config = codespan_reporting::term::Config::default();

    let lock = &mut writer.lock();

    for err in errors {
        codespan_reporting::term::emit(lock, &config, files, &err.into_diagnostic(interner))
            .unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_compiles(input: &str, output: &[u16]) {
        let bitcode = compile(input, "<test>").unwrap();
        assert_eq!(&bitcode, output);
    }

    #[test]
    fn db_directives() {
        test_compiles(
            r#"
            db 1, 0b111, 0o37, 0x42, "hi"
        "#,
            &[1, 0b111, 0o37, 0x42, b'h' as u16, b'i' as u16],
        );
    }

    #[test]
    fn constant() {
        test_compiles(
            r#"
            const a = 42
            const b = a
            const c = 38
            db a, b, c
        "#,
            &[42, 42, 38],
        );
    }

    #[test]
    fn labels() {
        test_compiles(
            r#"
            a:
            db 0, 0, 0, 0
            b: c: db 0, 0
            d:
            db a, b, c, d
        "#,
            &[0, 0, 0, 0, 0, 0, 0, 4, 4, 6],
        );
    }

    #[test]
    fn mov_operations() {
        test_compiles(
            r#"
            mov ac, br
            mov br, 5
            mov ix, (32)
        "#,
            &[0o001, 0o014, 5, 0o025, 32],
        );
    }
}
