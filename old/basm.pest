program = _{ SOI ~ (line? ~ NEWLINE)* ~ line? ~ NEWLINE? ~ EOI }
line = _{ ((label ~ line?) | constant | db_directive | operation) }

label = { identifier ~ ":" }
constant = { ^"const" ~ identifier ~ "=" ~ expression }
db_directive = { ^"db" ~ db_expression ~ ("," ~ db_expression)* }
db_expression = { string_literal | expression }

string_literal = @{ "\"" ~ ((!"\\" ~ !"\"" ~ ANY) | ("\\" ~ ANY))* ~ "\"" }

expression = { identifier | integer_literal }

integer_literal = { binary_literal | octal_literal | hex_literal | decimal_literal }
binary_literal = @{ ^"0b" ~ ("0"|"1"|"_")+ }
octal_literal = @{ ^"0o" ~ ('0'..'7' | "_")+ }
hex_literal = @{ ^"0x" ~ ('0'..'9' | 'A'..'F' | 'a'..'f' | "_")+ }
decimal_literal = @{ ('0'..'9' | "_")+ }

operation = { (implied_operation | unary_operation | binary_operation) ~ condition? }
condition = @{ "-" ~ ^"n"? ~ ^"c"? ~ ^"z"? }

implied_operation = { implied_instruction }
implied_instruction = { ^"hlt" | ^"ret" | ^"clc" | ^"clz" | ^"sec" | ^"sez" }

unary_operation = { unary_instruction ~ operand }
unary_instruction = { ^"add" | ^"adc" | ^"sbw" | ^"swb" | ^"nnd" | ^"and"
    | ^"aib" | ^"anb" | ^"bia" | ^"bna" | ^"ora" | ^"nor" | ^"jmp" | ^"jsr"
    | ^"dec" | ^"inc" | ^"xor" | ^"xnr" }

binary_operation = { (binary_instruction ~ operand ~ "," ~ operand) | mov_operation }
binary_instruction = { ^"sub" | ^"sbb" | ^"cmp" }

ref_start = _{ "(" }
ref_end = _{ ")" }
ref_ix_end = _{ "+" ~ ^"ix" ~ ")" }

operand_reference = { ref_start ~ operand ~ ref_end }
operand_reference_indexed = { ref_start ~ operand ~ ref_ix_end }

mov_operation = { (mov_instruction ~ operand ~ "," ~ operand)
    | (mov_instruction ~ direct_register ~ "," ~ operand_reference)
    | (mov_instruction ~ direct_register ~ "," ~ operand_reference_indexed)
    | (mov_instruction ~ operand_reference ~ "," ~ direct_register)
    | (mov_instruction ~ operand_reference_indexed ~ "," ~ direct_register) }
mov_instruction = { ^"mov" }

operand = { direct_register | immediate | immediate_reference | immediate_reference_indexed | ^"stack" }
immediate = { expression }
immediate_reference = { ref_start ~ expression ~ ref_end }
immediate_reference_indexed = { ref_start ~ expression ~ ref_ix_end }
direct_register = { ^"ac" | ^"br" | ^"ix" | ^"sp" }

keyword = { direct_register | mov_instruction | binary_instruction | unary_instruction | implied_instruction | ^"const" | ^"db" | ^"stack" }
identifier = @{ !keyword ~ XID_START ~ XID_CONTINUE* }

COMMENT = _{ ";" ~ (!NEWLINE ~ ANY)* }
WHITESPACE = _{ " " | "\t" }